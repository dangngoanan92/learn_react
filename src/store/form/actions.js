
// Action creator
export function actChangeInput(post) {
  return {
    type: "CHANGE_INPUT",
    payload: {
      post: post
    }
  }
}
export function actSubmitForm(post) {
    return {
      type: "SUBMIT_FORM",
      payload: {
        post: post
      }
    }
  }