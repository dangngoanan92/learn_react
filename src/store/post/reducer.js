import { ACT_ARTICLE_POSTS } from "./actions";

const initState = {
  articlesLatest: []}

function reducer(postState = initState, action) {
  switch (action.type) {
    case ACT_ARTICLE_POSTS:
      return {
        ...postState,
        listPosts: action.payload.posts
      }
    default:
      return postState
  }
}

export default reducer