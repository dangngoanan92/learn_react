import { api } from "../../services/api"
const url = '/wp/v2/posts?per_page=3&page=1'

export const ACT_ARTICLE_POSTS = 'ACT_ARTICLE_POSTS'

export function actFetchPosts(posts) {
    return {
      type: ACT_ARTICLE_POSTS,
      payload: { posts }
    }
  }

export const actArticleLated = () => {
    return dispatch => {
      api.call().get(url).then(reponse => {
        dispatch(actFetchPosts(reponse))
      })
    }
  }