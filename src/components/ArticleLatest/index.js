import './latest-news-list.css'
import ArticleItem from "../ArticleItem";
import MainTitle from '../shared/MainTitle'
import { useSelector }  from "react-redux"

function ArticleLatest() {
  const arrArticleLatest = useSelector(state => state.Post.listPosts)
  return (
    <div className="latest-news section">
			<div className="tcl-container">

				<MainTitle>Bài viết mới nhất</MainTitle>
        <div className="latest-news__list spacing">
            <div className="latest-news__card">
            <ArticleItem />
          </div>
    
        

        </div>
      </div>
    </div>
    
  )
}

export default ArticleLatest