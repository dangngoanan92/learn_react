import ArticleGeneral from "../components/ArticleGeneral";
import ArticleLatest from "../components/ArticleLatest";
import ArticlePopular from "../components/ArticlePopular";
import { actArticleLated } from "../store/post/actions"
import { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'

function HomePage() {
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(actArticleLated())
  }, [dispatch])

  return (
    <>
      <ArticleLatest />
      <ArticlePopular />
      <ArticleGeneral />
    </>
  )
}

export default HomePage